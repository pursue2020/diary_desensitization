# 设置新版本号
mvn versions:set -DnewVersion=1.0.0-SNAPSHOT
# 提交版本号变更
mvn versions:commit

`README的历史记录要点`
* 新增：增加的新功能描述
* 删除：删除的已有功能描述
* 修改：由什么功能修改为了什么功能
* 修复：修复了什么错误的功能
    
# commons-logback(Java语言开发)
# 1.0.0
* 初始化项目1.0.0

# 1.1.0
* 升级版本至1.1.0
* 修改包名和包路径(desensitization-logback,ch.qos.logback.desensitization)

# 1.1.1
* 升级版本至1.1.1
* 修复关键字匹配查找规则，主要修复问题：{"phoneNo":"11122223333"}会处理为{"phoneNo":"111*****333"}